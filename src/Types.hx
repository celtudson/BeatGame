package;

typedef Rect = {
	var w:Int;
	var h:Int;
	var x:Float;
	var y:Float;
}
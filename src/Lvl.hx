package;

import kha.graphics2.Graphics;
import kha.Image;
import kha.Assets;
import khm.Screen;

typedef Layer = {
	offx:Int,
	offy:Int,
	map:Array<Array<Int>>
}

typedef GameMap = {
	w:Int,
	h:Int,
	layers:Array<Layer>
}

class Lvl {

	var map:GameMap;
	public static var w:Int;
	public var h:Int;

	public static var vx:Float;
	public static var vy:Float;
	var vx_off:Int = 13 + 14;
	public static var maxVx:Int;
	public static var lockVx = false;
	var lockVy = false;

	var imgTileset:Image;

	public function new() {}

	public function init():Void {
		imgTileset = Assets.images.base;
		loadMap(0);
	}

	function loadMap(n:Int):Void {
		var json = Reflect.field(Assets.blobs, "map" + n + "_json").toString();
		map = haxe.Json.parse(json);
		w = map.w;
		h = map.h;

		vx = 0;
		maxVx = w * 64 - Screen.w + vx_off;
		if (maxVx < 32) maxVx = 32;
		vy = 0;
	}

	public function updateView(x:Float):Void {
		if (lockVx == false) {
			vx = Math.round(x + 32 - Screen.w / 2);
			if (vx < vx_off) vx = vx_off;
			if (vx > maxVx) vx = maxVx;
		}
	}

	public function render(g:Graphics):Void {
		var vx2 = vx_off - vx;

		for (i in 0...h)
		for (j in 0...w) {
			var id = map.layers[0].map[i][j] - 1;
			if (id < 0) continue;
			var yy = Math.floor(id / 2);
			var xx = id - yy * 2;
			g.drawSubImage(
				imgTileset, vx2 + j * 64, vy + i * 64,
				xx * 64, yy * 64, 64, 64
			);
		}

		for (j in 0...w + 1) {
			for (i in 0...2) {
				g.drawSubImage(
					imgTileset,
					vx2 + j * 64 - i * 32, 128 + i * 32,
					0, 64, 64, 32
				);
			}
		}
	}
}

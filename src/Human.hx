package;

import kha.graphics2.Graphics;
import kha.Image;
import Types;

@:enum
abstract States(Int) to Int {
	var walk = 0;
	var attack = 1;
	var stunned = 2;
	var retreat = 3;
}

class Human {

	public var id:Int;
	public var skinColor:Int = 0xFFFF0000;
	public var health:Float = 1;
	public var bodyRect:Rect;
	public var z:Float;
	public var walkSpeed:Float = 0;
	public var hspeed:Float;
	public var hspeedLock:Float = 0;
	public var vspeed:Float;
	public var g:Float;
	public var onLand:Bool;

	public var state:States;
	public var dir:Int = -1;
	public var nextPunch:Int = 0;
	public var combo_n:Int = 0;
	public var attackRect:Rect;
	public var attackMode:Int;
	public var prevSuccess:Bool = false;

	var anim_img:Image;
	var anim_noloop:Bool = false;
	public var frame_n:Float = 0;
	var frame_speed:Float = 0;
	var frame_max:Int;

	var imgWalk:Image;
	var imgRetreat:Image;
	var imgStunned0:Image;
	var imgStunned1:Image;
	var imgAttack0:Image;
	var imgAttack1:Image;
	var imgAttack2:Image;

	public function new() {}

	public function init(type:Int, newx:Int, newy:Int, newz:Int):Void {
		bodyRect = {w:8, h:48, x:newx, y:newy};
		z = newz;
		walkSpeed = 0;
		hspeed = 0;
		vspeed = 0;
		g = 0;
		onLand = false;
		state = States.walk;
		attackRect = {w:12, h:48, x:newx, y:newy};

		switch (type) {
		case 0:
			imgWalk = Load.char0_walk;
			/*imgRetreat = Load.char0_retreat;
			imgStunned0 = Load.char0_stunned0;
			imgStunned1 = Load.char0_stunned1;
			imgAttack0 = Load.char0_attack0;
			imgAttack1 = Load.char0_attack1;
			imgAttack2 = Load.char0_attack2;*/
		}
		anim_img = imgWalk;
	}

	public function changeState(newState:States):Void {
		state = newState;
		frame_n = 0;
		frame_max = 4;
		anim_noloop = false;

		switch (state) {
		case States.walk:
			anim_img = imgWalk;
			frame_speed = 1 / 12;
			frame_max = 4;
			combo_n = 0;
			hspeedLock = 0;
		case States.retreat:
			/*anim_noloop = true;
			anim_img = imgRetreat;
			frame_speed = 1 / 6;
			frame_max = 5;*/
		case States.stunned:
			/*anim_noloop = true;
			frame_speed = 1 / 6;
			frame_max = 2;
			combo_n = 0;
			switch (attackMode) {
			case 0: anim_img = imgStunned0;
			case 1: anim_img = imgStunned1;
			case 2: anim_img = imgStunned1;
			case 3: anim_img = imgStunned1;
			}*/
		case States.attack:
			/*attackMode = 0;
			frame_speed = 1 / 6;
			switch (combo_n) {
			case 1: anim_img = imgAttack0;
			case 2:
				attackMode = 1;
				anim_img = imgAttack1;
			case 3:
				attackMode = 2;
				anim_img = imgAttack2;
				frame_max = 5;
			}*/
		}
	}

	public function comboTick():Void {
		frame_n += frame_speed;
		if (nextPunch == 0) return;

		if (nextPunch == 1) {
			nextPunch = 0;
			state = States.retreat;
			frame_n = 0;
			hspeedLock = 30;
			hspeed = -3 * dir;
			Lvl.lockVx = true;
			return;
		}
		
		if (state == States.retreat && hspeedLock == 0) {
			nextPunch = 0;
			state = States.attack;
			frame_n = 0;
			switch (combo_n) {
			case 0: //A,B
				hspeedLock = 48;
				hspeed = 4 * dir;
			case 1: //B,A,B
			case 2: //B,B,A,B
			}
		} else if (state == States.walk ||
		(state == States.attack && prevSuccess == true && frame_n >= 3)) {
			nextPunch = 0;
			prevSuccess = false;
			state = States.attack;
			hspeedLock = 0;

			if (combo_n < 3) {
				combo_n++;
				frame_n = 0;
				frame_speed = 0;
				if (dir == -1) {
					attackRect.x = bodyRect.x - attackRect.w;
				} else {
					attackRect.x = bodyRect.x + bodyRect.w;
				}
				attackRect.y = bodyRect.y;
			}
		}
	}

	public function update():Void {
		if (frame_n >= frame_max) {
			if (anim_noloop == false) {
				frame_n = 0;
			} else {
				frame_n = frame_max - 1;
			}
			if (state == States.attack) {
				state = States.walk;
				frame_n = 0;
				combo_n = 0;
				prevSuccess = false;
			} else if (state == States.retreat) {
				state = States.walk;
				frame_n = 0;
			}
		}
		if (frame_n == 0) changeState(state);

		if (state == States.walk) {
			hspeed = walkSpeed;
		} else {
			walkSpeed = 0;
		}
		bodyRect.x += hspeed;
		if (bodyRect.x < 22) bodyRect.x = 22;
		if (bodyRect.x > Lvl.w * 64 - 32) bodyRect.x = Lvl.w * 64 - 32;

		if (hspeedLock == 0) {
			hspeed = 0;
		} else {
			hspeedLock -= Math.abs(hspeed);
			if (hspeedLock <= 0) {
				hspeedLock = 0;
				hspeed = 0;
			}
		}
		if (state == States.retreat && hspeedLock == 0) {
			Lvl.lockVx = false;
		}

		g += 0.2;
		vspeed += g;
		if (vspeed > 6) vspeed = 6;
		bodyRect.y += vspeed;
		onLand = false;
		if (bodyRect.y >= 68) {
			bodyRect.y = 68;
			onLand = true;
		}
		if (state == States.stunned && onLand == true && hspeedLock == 0) changeState(States.walk);

		if (walkSpeed < 0) dir = -1;
		if (walkSpeed > 0) dir = 1;
		walkSpeed = 0;
	}

	public function render(g:Graphics):Void {
		g.color = 0xFFFF0000;
		//g.fillRect(bodyRect.x - Lvl.vx, bodyRect.y - Lvl.vy, 64, 64);

		var n = Math.floor(frame_n);
		var scale = 1;
		var offx = 0;
		if (dir < 0) {
			scale = -1;
			offx = -64;
		}

		g.color = skinColor;
		g.drawScaledSubImage(
			anim_img, n * 64, 0, 64, 64,
			bodyRect.x - Lvl.vx - offx, bodyRect.y - Lvl.vy + z, 64 * scale, 64
		);
		g.color = 0x9000FF00;
		//g.fillRect(bodyRect.x - Lvl.vx + 28, bodyRect.y - Lvl.vy + 16 + z, bodyRect.w, bodyRect.h);
		/*if (state == States.attack && frame_n >= 2 && frame_n < 2.1) {
			g.color = 0x90FF0000;
			g.fillRect(attackRect.x - Lvl.vx + 14, attackRect.y - Lvl.vy + 4, attackRect.w, attackRect.h);
		}*/
		g.color = 0xFFFFFFFF;
		//g.drawString("" + state, bodyRect.x - Lvl.vx + 12, bodyRect.y - Lvl.vy - 12 + z);
	}
}
